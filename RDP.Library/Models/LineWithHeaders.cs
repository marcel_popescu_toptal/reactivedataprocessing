﻿namespace RDP.Library.Models
{
    public class LineWithHeaders
    {
        public string[] Headers { get; private set; }
        public string Line { get; private set; }

        public LineWithHeaders(string[] headers, string line)
        {
            Headers = headers;
            Line = line;
        }
    }
}