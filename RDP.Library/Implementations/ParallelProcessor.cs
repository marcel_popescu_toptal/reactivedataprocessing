﻿using System;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using RDP.Library.Interfaces;
using RDP.Library.Models;

namespace RDP.Library.Implementations
{
    public class ParallelProcessor
    {
        public int ThreadCount { get; set; }

        public ParallelProcessor(IScheduler scheduler, TextLoader loader, Splitter<string> splitter)
        {
            this.scheduler = scheduler;
            this.loader = loader;
            this.splitter = splitter;
        }

        public IObservable<string> Process(string url, int headerSize, LineProcessor lineProcessor)
        {
            var lines = loader.Read(url);

            // need to use Publish here because otherwise the stream will be enumerated twice
            return lines.Publish(shared =>
            {
                var header = shared.Take(headerSize).ToArray();
                var rest = shared.Skip(headerSize);

                var streams = splitter.Split(rest, ThreadCount);

                // using SubscribeOn instead of ObserveOn because processing starts immediately when subscribing
                return header
                    .SelectMany(h => streams
                        .Select(stream => ProcessLines(stream, h, lineProcessor)
                            .SubscribeOn(scheduler)))
                    .Merge();
            });
        }

        //

        private readonly IScheduler scheduler;
        private readonly TextLoader loader;
        private readonly Splitter<string> splitter;

        private static IObservable<string> ProcessLines(IObservable<string> observable, string[] header, LineProcessor lineProcessor)
        {
            return observable
                .Select(line => new LineWithHeaders(header, line))
                .SelectMany(lineProcessor.Process);
        }
    }
}