﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using RDP.Library.Interfaces;

namespace RDP.Library.Implementations
{
    public class ProducerConsumerSplitter<T> : Splitter<T>
    {
        public IObservable<T>[] Split(IObservable<T> observable, int count)
        {
            // block if the collection grows past (thread count * 10) items
            var collection = new BlockingCollection<T>(count * 10);

            observable.Subscribe(collection.Add, () => collection.CompleteAdding());

            return Enumerable
                .Range(0, count)
                .Select(_ => CreateConsumer(collection))
                .ToArray();
        }

        //

        private static IObservable<T> CreateConsumer(BlockingCollection<T> collection)
        {
            return Observable.Create<T>(o =>
            {
                while (!collection.IsCompleted)
                {
                    T item;
                    if (collection.TryTake(out item))
                        o.OnNext(item);
                }
                o.OnCompleted();

                return Disposable.Empty;
            });
        }
    }
}