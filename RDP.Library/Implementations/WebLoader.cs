﻿using System;
using System.IO;
using System.Net;
using RDP.Library.Interfaces;

namespace RDP.Library.Implementations
{
    public class WebLoader : UrlLoader
    {
        public Stream Open(string url)
        {
            var req = WebRequest.Create(url);

            var httpRequest = req as HttpWebRequest;
            var fileRequest = req as FileWebRequest;

            if (httpRequest != null)
                SetUpHttpRequest(httpRequest);
            else if (fileRequest != null)
                SetUpFileRequest(fileRequest);
            else
                throw new Exception("Unknown request type for " + url);

            return new WebStream(req);
        }

        //

        private const int DEFAULT_TIMEOUT = 30 * 1000; // 30 seconds

        private static void SetUpHttpRequest(HttpWebRequest req)
        {
            req.Method = WebRequestMethods.Http.Get;
            req.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            req.Timeout = DEFAULT_TIMEOUT;
            req.ReadWriteTimeout = DEFAULT_TIMEOUT;
            req.ContinueTimeout = DEFAULT_TIMEOUT;
            req.ProtocolVersion = HttpVersion.Version11;
            req.KeepAlive = false;
        }

        // ReSharper disable once UnusedParameter.Local
        private static void SetUpFileRequest(FileWebRequest req)
        {
            // nothing to do
        }
    }
}