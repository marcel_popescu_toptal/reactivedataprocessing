﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using RDP.Library.Interfaces;

namespace RDP.Library.Implementations
{
    public class RemoteTextLoader : TextLoader
    {
        public RemoteTextLoader(IScheduler scheduler, UrlLoader loader)
        {
            this.scheduler = scheduler;
            this.loader = loader;
        }

        public IObservable<string> Read(string url)
        {
            return Observable.Using(
                () => new StreamReader(loader.Open(url)),
                sr => ReadLoop(sr).ToObservable(scheduler));
        }

        //

        private readonly IScheduler scheduler;
        private readonly UrlLoader loader;

        private static IEnumerable<string> ReadLoop(StreamReader reader)
        {
            while (!reader.EndOfStream)
                yield return reader.ReadLine();
        }
    }
}