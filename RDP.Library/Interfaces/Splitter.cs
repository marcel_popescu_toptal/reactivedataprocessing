﻿using System;

namespace RDP.Library.Interfaces
{
    public interface Splitter<T>
    {
        /// <summary>
        ///     Splits the given stream into a number of streams.
        /// </summary>
        /// <param name="observable">The stream to be splitted.</param>
        /// <param name="count">The number of streams to return.</param>
        /// <returns>An array of streams.</returns>
        IObservable<T>[] Split(IObservable<T> observable, int count);
    }
}