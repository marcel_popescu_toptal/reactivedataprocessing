﻿using System;

namespace RDP.Library.Interfaces
{
    public interface TextLoader
    {
        /// <summary>
        ///     Opens a text file (local or remote) and returns a stream of lines.
        /// </summary>
        /// <param name="url">The file location.</param>
        /// <returns>An observable of each line from the given resource.</returns>
        IObservable<string> Read(string url);
    }
}