﻿using RDP.Library.Models;

namespace RDP.Library.Interfaces
{
    public interface LineProcessor
    {
        string[] Process(LineWithHeaders lwh);
    }
}