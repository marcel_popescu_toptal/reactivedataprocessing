﻿using System.IO;

namespace RDP.Library.Interfaces
{
    public interface UrlLoader
    {
        /// <summary>
        ///     Opens a remote resource and returns it as a stream.
        /// </summary>
        /// <param name="url">The address of the resource.</param>
        /// <returns>A stream representing the given resource.</returns>
        Stream Open(string url);
    }
}