﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;
using RDP.Library.Implementations;

namespace RDP.Tester
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class Program
    {
        private const int THREAD_COUNT = 4;

        //private const string URL = "http://download.bls.gov/pub/time.series/la/la.data.0.CurrentU00-04";
        private const string URL = @"file:///S:\toptal\ReactiveDataProcessing\_Data\large.txt";

        // this one is small
        //private const string URL = "http://download.bls.gov/pub/time.series/la/la.data.5.RegionDivisionS";

        // testing error handling
        //private const string URL = "http://999.999.999.999:1234/index.htm";

        private static void Main()
        {
            var resourceLoader = new RemoteTextLoader(NewThreadScheduler.Default, new WebLoader());
            var splitter = new ProducerConsumerSplitter<string>();
            var processor = new ParallelProcessor(NewThreadScheduler.Default, resourceLoader, splitter) { ThreadCount = THREAD_COUNT };

            // surprisingly, this proved faster than opening a buffered FileStream and using a StreamWriter
            using (var file = File.CreateText("output.txt"))
            {
                var count = 0;

                // 1. nothing is actually executed until we subscribe to the stream (or await it)
                // 2. the Select runs on a single stream - the processing threads have been merged
                var stream = processor
                    .Process(URL, 1, new SampleProcessor())
                    .Do(line =>
                    {
                        // ReSharper disable once AccessToDisposedClosure
                        file.WriteLine(line);
                        count++;
                    });

                Console.WriteLine("Starting...");
                var elapsed = BenchmarkAsync(stream).Result;
                Console.WriteLine("Processing done.");

                Console.WriteLine($"Elapsed: {elapsed}, Count: {count}, TPS: {count / elapsed.TotalMilliseconds * 1000}");
            }
        }

        private static async Task<TimeSpan> BenchmarkAsync<T>(IObservable<T> observable)
        {
            var sw = new Stopwatch();

            sw.Start();

            // the actual processing happens here
            try
            {
                await observable;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }

            sw.Stop();

            return sw.Elapsed;
        }
    }
}