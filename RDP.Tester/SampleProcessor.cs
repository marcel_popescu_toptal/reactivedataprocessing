﻿using System;
using System.Text;
using System.Threading;
using RDP.Library.Interfaces;
using RDP.Library.Models;

namespace RDP.Tester
{
    public class SampleProcessor : LineProcessor
    {
        public string[] Process(LineWithHeaders lwh)
        {
            var data = lwh.Line.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

            if (!HEADER.IsValueCreated)
                HEADER.Value = lwh.Headers[0].Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

            SB.Value.Clear();
            SB.Value.Append(HEADER.Value[HEADER.Value.Length - 1]);
            SB.Value.Append("=");
            SB.Value.Append(data[data.Length - 1]);

            return new[] { SB.ToString() };

            // I cannot find a significant time difference between these two methods
            //var header = lwh.Headers[0].Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            //return new[] { header[header.Length - 1] + "=" + data[data.Length - 1] };
        }

        //

        private static readonly ThreadLocal<string[]> HEADER = new ThreadLocal<string[]>();
        private static readonly ThreadLocal<StringBuilder> SB = new ThreadLocal<StringBuilder>(() => new StringBuilder());
    }
}