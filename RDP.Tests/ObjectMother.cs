﻿using System.Collections.Generic;

namespace RDP.Tests
{
    public static class ObjectMother
    {
        public static List<T>[] CreateLists<T>(int count)
        {
            var result = new List<T>[count];
            for (var i = 0; i < count; i++)
                result[i] = new List<T>();

            return result;
        }
    }
}