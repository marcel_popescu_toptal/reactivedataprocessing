﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Linq;
using System.Text;
using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RDP.Library.Implementations;
using RDP.Library.Interfaces;

namespace RDP.Tests.Implementations
{
    [TestClass]
    public class RemoteTextLoaderTests
    {
        private const string URL = "";

        private TestScheduler scheduler;
        private Mock<UrlLoader> loader;

        private RemoteTextLoader sut;

        [TestInitialize]
        public void SetUp()
        {
            scheduler = new TestScheduler();
            loader = new Mock<UrlLoader>();

            sut = new RemoteTextLoader(scheduler, loader.Object);
        }

        [TestMethod]
        public void CompletesTheObservableIfTheStreamIsEmpty()
        {
            using (var ms = new MemoryStream())
            {
                loader
                    .Setup(it => it.Open(URL))
                    .Returns(ms);
                var list = new List<string>();

                sut
                    .Read(URL)
                    .Subscribe(list.Add);
                scheduler.AdvanceBy(100);

                Assert.AreEqual(0, list.Count);
            }
        }

        [TestMethod]
        public void ProducesTheEntireTextIfSingleLine()
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes("single line")))
            {
                loader
                    .Setup(it => it.Open(URL))
                    .Returns(ms);
                var list = new List<string>();

                sut
                    .Read(URL)
                    .Subscribe(list.Add);
                scheduler.AdvanceBy(100);

                Assert.AreEqual(1, list.Count);
                Assert.AreEqual("single line", list[0]);
            }
        }

        [TestMethod]
        public void ProducesMultipleLines()
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes("line1\r\nline2\r\n")))
            {
                loader
                    .Setup(it => it.Open(URL))
                    .Returns(ms);
                var list = new List<string>();

                sut
                    .Read(URL)
                    .ObserveOn(scheduler)
                    .Subscribe(list.Add);
                scheduler.AdvanceBy(100);

                Assert.AreEqual(2, list.Count);
                Assert.AreEqual("line1", list[0]);
                Assert.AreEqual("line2", list[1]);
            }
        }

        [TestMethod]
        public void TheSubscriptionEndsWhenTheStreamIsClosed()
        {
            var closed = false;
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes("some text")))
            {
                loader
                    .Setup(it => it.Open(URL))
                    .Returns(ms);
                sut
                    .Read(URL)
                    .Subscribe(s => { }, () => closed = true);
                scheduler.AdvanceBy(100);
            }

            Assert.IsTrue(closed);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void DisposesTheStreamIfSubscriptionEndsPrematurely()
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes("line1\r\nline2\r\n")))
            {
                loader
                    .Setup(it => it.Open(URL))
                    .Returns(ms);
                sut
                    .Read(URL)
                    .Take(1)
                    .Subscribe(s => { });
                scheduler.AdvanceBy(100);

                // the stream should already have been disposed at this point; trying to access the Position property should throw
                var position = ms.Position;
            }
        }
    }
}