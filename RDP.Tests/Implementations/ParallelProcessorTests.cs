﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RDP.Library.Implementations;
using RDP.Library.Interfaces;
using RDP.Library.Models;

namespace RDP.Tests.Implementations
{
    [TestClass]
    public class ParallelProcessorTests
    {
        private readonly Mock<TextLoader> loader = new Mock<TextLoader>();

        [TestMethod]
        public void ProcessesEverythingSequentially()
        {
            const string URL = "";

            var scheduler = NewThreadScheduler.Default; // use the real scheduler
            var splitter = new ProducerConsumerSplitter<string>();

            loader
                .Setup(it => it.Read(URL))
                .Returns(new[] { "Header", "Line1", "Line2", "Line3" }.ToObservable());
            var list = new List<string>();
            var threads = new List<int>();
            var completed = new ManualResetEvent(false);

            var sut = new ParallelProcessor(scheduler, loader.Object, splitter) { ThreadCount = 1 };

            var result = sut.Process(URL, 1, new TestProcessor());
            result.Subscribe(
                s =>
                {
                    list.Add(s);
                    threads.Add(Thread.CurrentThread.ManagedThreadId);
                },
                () => completed.Set());
            completed.WaitOne();

            CollectionAssert.AreEqual(new[] { "Header", "Line1", "Header", "Line2", "Header", "Line3" }, list);

            // verify that there is only one thread
            Assert.IsTrue(threads.Distinct().Count() == 1);
        }

        [TestMethod]
        public void ProcessesEverythingInParallel()
        {
            const string URL = "";

            var scheduler = NewThreadScheduler.Default; // use the real scheduler
            var splitter = new ProducerConsumerSplitter<string>();

            loader
                .Setup(it => it.Read(URL))
                .Returns(new[] { "Header", "1", "2", "3", "4", "5" }.ToObservable());
            var list = new List<string>();
            var threads = new List<int>();
            var completed = new ManualResetEvent(false);

            var sut = new ParallelProcessor(scheduler, loader.Object, splitter) { ThreadCount = 5 };

            var result = sut.Process(URL, 1, new TestProcessor());
            result.Subscribe(
                s =>
                {
                    list.Add(s);
                    threads.Add(Thread.CurrentThread.ManagedThreadId);
                },
                () => completed.Set());
            completed.WaitOne();

            // using AreEquivalent instead of AreEqual because the ordering is not always preserved, due to the parallel processing
            CollectionAssert.AreEquivalent(new[] { "Header", "1", "Header", "2", "Header", "3", "Header", "4", "Header", "5" }, list);

            // verify that there is more than one thread
            Assert.IsTrue(threads.Distinct().Count() > 1);
        }

        //

        private class TestProcessor : LineProcessor
        {
            public string[] Process(LineWithHeaders lwh)
            {
                return lwh.Headers.Concat(new[] { lwh.Line }).ToArray();
            }
        }
    }
}