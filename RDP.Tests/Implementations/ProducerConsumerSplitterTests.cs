﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RDP.Library.Implementations;

namespace RDP.Tests.Implementations
{
    [TestClass]
    public class ProducerConsumerSplitterTests
    {
        private readonly TestScheduler scheduler = new TestScheduler();

        private ProducerConsumerSplitter<int> sut;

        [TestMethod]
        public void ReturnsSingleStreamIdenticalToSource()
        {
            sut = new ProducerConsumerSplitter<int>();

            var source = new[] { 1, 2, 3 }.ToObservable();
            var list = new List<List<int>>();

            var results = sut.Split(source, 1);
            foreach (var result in results)
            {
                var inner = new List<int>();
                list.Add(inner);

                result.Subscribe(inner.Add);
            }
            scheduler.AdvanceBy(100);

            Assert.AreEqual(1, list.Count);
            CollectionAssert.AreEqual(new[] { 1, 2, 3 }, list[0].ToArray());
        }

        [TestMethod]
        public void ReturnsTwoStreams()
        {
            sut = new ProducerConsumerSplitter<int>();

            var source = new[] { 1, 2, 3 }.ToObservable();
            var list = new List<List<int>>();

            var results = sut.Split(source, 2);
            foreach (var result in results)
            {
                var inner = new List<int>();
                list.Add(inner);

                result.Subscribe(inner.Add);
            }
            scheduler.AdvanceBy(100);

            Assert.AreEqual(2, list.Count);
            // the way the items are distributed is rather random
        }

        [TestMethod]
        public void TheIndividualStreamsCompleteWhenTheInputStreamEnds()
        {
            sut = new ProducerConsumerSplitter<int>();

            var source = new[] { 1, 2, 3 }.ToObservable();
            var completed = new bool[2];
            completed[0] = false;
            completed[1] = false;

            var results = sut.Split(source, 2);
            results[0].Subscribe(_ => { }, () => completed[0] = true);
            results[1].Subscribe(_ => { }, () => completed[1] = true);
            scheduler.AdvanceBy(100);

            Assert.IsTrue(completed[0]);
            Assert.IsTrue(completed[1]);
        }
    }
}